from django.db import models

# Create your models here.
class Product(models.Model):
    HISTORY_MAX_ITEMS = 5 # Numbers of logs we save in history for a same product. We delete older items.

    created = models.DateTimeField(auto_now_add=True)
    asin = models.CharField(max_length=25)
    url = models.URLField()
    title = models.CharField(max_length=50)
    should_watch = models.BooleanField(default=True) # If the product doesnt exist anymore, will stop watching it.

    class Meta:
        ordering = ['title', 'created']
