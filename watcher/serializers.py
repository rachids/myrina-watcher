from rest_framework import serializers

from watcher.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'title', 'asin', 'url', 'should_watch']
