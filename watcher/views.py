from rest_framework import viewsets, generics, permissions

from watcher.models import Product
from watcher.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    # Check here : https://www.django-rest-framework.org/tutorial/3-class-based-views/
    # Need to be able to verify that a product added is existing in Amazon before inserting it in DB.
    # Use a validator maybe is better.
    # Need to abstract the call to Amazon.
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]
